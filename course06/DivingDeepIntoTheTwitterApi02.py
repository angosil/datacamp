# Import package
import tweepy
from course06 import DivingDeepIntoTheTwitterApi as ddta
from MyStreamListener.tweet_listener import MyStreamListener

# Initialize Stream listener
l = MyStreamListener()

# Create you Stream object with authentication
stream = tweepy.Stream(ddta.auth, l)

# Filter Twitter Streams to capture data by the keywords:
# stream.filter(track=['clinton', 'trump', 'sanders', 'cruz'])

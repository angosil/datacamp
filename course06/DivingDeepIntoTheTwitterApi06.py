# Import packages
from course06 import DivingDeepIntoTheTwitterApi05 as ddta
import matplotlib.pyplot as plt
import seaborn as sns


# Set seaborn style
sns.set(color_codes=True)

# Create a list of labels:cd
cd = ['clinton', 'trump', 'sanders', 'cruz']

# Plot histogram
ax = sns.barplot(cd, [ddta.clinton, ddta.trump, ddta.sanders, ddta.cruz])
ax.set(ylabel="count")
plt.show()

# Import pandas
import pandas as pd
# Import matplotlib
import matplotlib.pyplot as plt

# Assign spreadsheet filename: file
file = 'TREND01-5G-educ-fertility-bubbles.xls'

# Load spreadsheet: xl
xl = pd.ExcelFile(file)

# Print sheet names
print(xl.sheet_names)

# Data Frame "df" head
df = xl.parse("data COMPILATION", skiprows=7)
print(df.head())

# Data frame "df" columns and shape
print('-------(Columns and Shape:)--------')
print(df.columns, df.shape)

# Data frame "df" info
print('-------(Info:)--------')
print(df.info())

# Data frame "df"  'continent' count values
print('-------(df.continent Value Count:)--------')
print(df.Continent.value_counts(dropna=False))

print('-------(df.continent Value Count Head:)--------')
print(df.Continent.value_counts(dropna=False).head())

print('-------(df.Country Value Count Head:)--------')
print(df['Country '].value_counts(dropna=False).head())

print('-------(df.fertility Value Count Head:)--------')
print(df.fertility.value_counts(dropna=False).head())

# Data frame "df" describe
print('-------(Describe:)--------')
print(df.describe())

# Data frame "df" plot
print('-------(Plot:)--------')
df.population.plot(kind='hist')
plt.show()

df.boxplot(column='population', by='Country ', rot=90)
plt.show()

# Import pandas
import pandas as pd
# Import matplotlib
import matplotlib.pyplot as plt

# Read the file into a DataFrame: df
df = pd.read_csv('dob_job_application_filings.csv')

# Print the head of df
print('-------(Head:)--------')
print(df.head())

# Print the tail of df
print('-------(Tail:)--------')
print(df.tail())

# Print the shape of df
print('-------(shape:)--------')
print(df.shape)

# Print the columns of df
print('-------(Columns:)--------')
print(df.columns)

# Print the info of df
print('-------(Info:)--------')
print(df.info())

# Data frame "df" describe
print('-------(Describe:)--------')
print(df.describe())

# Print the value counts for 'Borough'
print('value counts for "Borough"')
print(df['Borough'].value_counts(dropna=False))

# Print the value_counts for 'State'
print('value counts for "State"')
print(df['State'].value_counts(dropna=False))

# Create the boxplot
df.boxplot(column='Initial Cost', by='Borough', rot=90)
plt.show()

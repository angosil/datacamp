# Import pandas
import pandas as pd

# Assign spreadsheet filename: file
file = 'ebola_data_db_format.csv'

# Read the file into a DataFrame: df
ebola_df = pd.read_csv(file)

print(ebola_df.info())

# Assert that there are no missing values
assert ebola_df.notnull().all().all()

assert (ebola_df >= 0).all().all()

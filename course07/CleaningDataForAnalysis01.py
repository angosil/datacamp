# Import the regular expression module
import re

# Regex
# 17        12345678901         \d*
# $17       $12345678901        \$\d*
# $17.00    $12345678901.42     \$\d*\.\d*
# $17.98    $12345678901.24     \$\d*\.\d*{2}

# Compile the pattern: prog
prog = re.compile('\d{3}-\d{3}-\d{4}')

# See if the pattern matches
result = prog.match('123-456-7890')
print(bool(result))

# See if the pattern matches
result = prog.match('1123-456-7890')
print(bool(result))

# Find the numeric values: matches
text = 'the recipe calls for 10 strawberries and 1 banana'
matches = re.findall('\d+', text)

# Print the matches
print(matches)

# Write the first pattern
pattern1 = bool(re.match(pattern='\d{3}-\d{3}-\d{4}', string='123-456-7890'))
print(pattern1)

# Write the second pattern
pattern2 = bool(re.match(pattern='\$\d*\.\d*', string='$123.45'))
print(pattern2)

# Write the third pattern
pattern3 = bool(re.match(pattern='[A-Z]\w*', string='Australia'))
print(pattern3)



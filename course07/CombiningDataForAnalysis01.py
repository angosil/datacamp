from sqlalchemy import create_engine
import pandas as pd

# Create engine: engine
engine = create_engine('sqlite:///survey.db')


def get_data_frame(connection, table_name: str):
    """
    Return Data Frame from sql table
    :param connection: SQL connection
    :param table_name: Table string name
    :rtype: pandas.core.frame.DataFrame
    """
    table = connection.execute("SELECT * FROM {}".format(table_name))
    df = pd.DataFrame(table.fetchall())
    df.columns = table.keys()
    return df


# Open engine in context manager
# Perform query and save results to DataFrame: df
with engine.connect() as con:
    site_df = get_data_frame(con, 'Site')
    visited_df = get_data_frame(con, 'Visited')
    survey_df = get_data_frame(con, 'Survey')

# Print head of DataFrame df
print(site_df)

# Print head of DataFrame df
print(visited_df)

# Print head of DataFrame df
print('-----(Survey DF)-----')
print(survey_df)

# Merged data frames
print('-----(Merge Site and Visited DF)-----')
mer_df = pd.merge(left=site_df, right=visited_df, on=None, left_on='name', right_on='site')
print('-----(survey_df.dtypes)-----')
print(mer_df.dtypes)
print('-----(survey_df.info())-----')
print(survey_df.info())
print('-----(mer_df.info())-----')
mer_df['id'] = mer_df['id'].astype('int64')
print(mer_df.info())
print('-----(Merge Site and Visited DF)-----')
mer_df = pd.merge(left=mer_df, right=survey_df, on=None, left_on='id', right_on='taken')
print(mer_df)

# Import pandas
import pandas as pd


# Assign spreadsheet filename: file
file = 'ebola_data_db_format.csv'

# Read the file into a DataFrame: df
df = pd.read_csv(file)

# Print the head of df
print('-------(Head:)--------')
print(df.head())

# Print the tail of df
print('-------(Tail:)--------')
print(df.tail())

# Print the columns of df
print('-------(Columns:)--------')
print(df.columns)

# Print the info of df
print('-------(Info:)--------')
print(df.info())

# Data frame "df" describe
print('-------(Describe:)--------')
print(df.describe())

# Print the value counts for 'Country'
print('value counts for "Country"')
print(df.Country.value_counts(dropna=False))

# Print the value counts for 'Date'
print('value counts for "Date"')
print(df.Date.value_counts(dropna=False))

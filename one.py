import matplotlib.pyplot as plt
import numpy as np

np.random.seed(123)

# Initialize all_walks
all_walks = []

# Simulate random walk 500 times
for i in range(500):

    # Initialize random_walk
    random_walk = [0]

    for x in range(100):

        # Set step: last element in random_walk
        step = random_walk[x]

        # Roll the dice
        dice = np.random.randint(1, 7)

        # Finish the control construct
        if dice <= 2:
            step = max(0, step - 1)
        elif dice <= 5:
            step = step + 1
        else:
            step = step + np.random.randint(1, 7)

        # Implement clumsiness
        if np.random.rand() <= 0.001:
            step = 0

        # append next_step to random_walk
        random_walk.append(step)

    # Append random_walk to all_walks
    all_walks.append(random_walk)

# Convert all_walks to Numpy array: np_aw
np_aw = np.array(all_walks)

# Transpose np_aw: np_aw_t
np_aw_t = np.transpose(np_aw)

# Select last row from np_aw_t: ends
ends = np_aw_t[-1, :]

# Plot histogram of ends, display plot
plt.hist(ends)
plt.show()

print(ends.size)
print(ends[ends >= 60].size/500)

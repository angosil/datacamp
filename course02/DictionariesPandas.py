import pandas as pd
import numpy as np

data = {
    "country": ["Brazil", "Russia", "India", "China", "South Africa"],
    "capital": ["Brasilia", "Moscow", "New Delhi", "Beijing", "Pretoria"],
    "area": [8.516, 17.10, 3.286, 9.597, 1.221],
    "population": [200.4, 143.5, 1252, 1355, 52.98]
}

brics = pd.DataFrame(data)

print(brics)

print(brics[1:4])

for i, x in brics.iterrows():
    print(str(i) + " : " + str(x['area']))

# brics.set_index(["A", "B", "C", "D"])
# print(brics)

print("brics.iloc[:, 3]")
print(brics.iloc[:, 3])
print()
print(brics.loc[:, "capital"])
print()
print(brics.loc[[3], 'area'])
print()
print(brics[np.logical_not(brics['area'] > 5)])
print()
print(brics.loc[:, ["country", "capital"]])
print()
for i, q in brics.iterrows():
    brics.loc[i, 'COUNTRY'] = q['country'].upper()
print(brics)



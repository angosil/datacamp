import numpy as np

x = np.array([[1, 2, 7], [7, 7, 1]])
y = np.array([[6, 5, 5], [0, 6, 1]])

print(x/y)


y = "the book is on the table"
print(y.upper())

x = ['h', 'o', 'b', 'q', 'p', 'p']
print(x[3] + x[5])

m = np.array([6, 2, 4])
n = np.array([2, True, False])
print(m ** n)

x = np.array([[4, 5, 6],
              [14, 15, 16]])

print(np.transpose(x))

print(not(73 < 32))

costs = [17, 9, 8, 11]
print(np.array(costs) > 18)

print(y.count("i"))

p = [[3, "A", 5], [2, 6, "B"], ["C", "D", "E"]]
print(p[1][0])

z = "education in the word"
print(z.replace("i", "_"))
print(z.count('e'))

x = np.array([3, 4, False, True, "5.2"])
print(x)

import pandas as pd
import numpy as np

date_range_1 = pd.date_range('2016-07-01', periods=17, freq='D')
print(date_range_1)
ts1 = pd.Series([x for x in range(0, 17)], index=date_range_1)
print(ts1)

date_range_2 = pd.date_range('2016-07-01', periods=11, freq='B')
print(date_range_2)
ts2 = pd.Series([x for x in range(0, 11)], index=date_range_2)
print(ts2)

print('-----------------EJERCICIO-----------------')

# Reset the index of ts2 to ts1, and then use linear interpolation to fill in the NaNs: ts2_interp
ts2_interp = ts2.reindex(ts1.index).interpolate(how='linear')

# Compute the absolute difference of ts1 and ts2_interp: differences
differences = np.abs(ts1 - ts2_interp)

# # Generate and print summary statistics of the differences
print(ts2_interp, differences, differences.describe())

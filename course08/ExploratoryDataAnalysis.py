import pandas as pd
import matplotlib.pyplot as plt


file_messy = "messy_stock_data.csv"
df = pd.read_csv(
    file_messy,
    header=0
)
print(df.head())
df = df.loc[[3, 2, 0]].set_index('name').T
# df.columns = ['', 'AAPL', 'GOOG', 'IBM']
df = df.rename(index=str, columns={"name": "Month", "APPLE": "AAPL", "GOOGLE": "GOOG", "IBM": "IBM"})
print(df.head())

# Create a list of y-axis column names: y_columns
y_columns = ["AAPL", "IBM"]

# Generate a line plot
df.plot(y=y_columns)

# Add the title
plt.title('Monthly stock prices')

# Add the y-axis label
plt.ylabel('Price ($US)')

# Display the plot
plt.show()

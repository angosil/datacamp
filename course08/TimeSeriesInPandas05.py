import pandas as pd
import matplotlib.pyplot as plt

file = "austin_airport_departure_data_2015_july.csv"
df = pd.read_csv(
    file,
    header=10,
    parse_dates=True,
    index_col="Date (MM/DD/YYYY)"
)
print(df.head())

print('-----------------EJERCICIO-----------------')

# Strip extra whitespace from the column names: df.columns
df.columns = df.columns.str.strip()

# Extract data for which the destination airport is Dallas: dallas
dallas = df['Destination Airport'].str.contains('DAL')

# Compute the total number of Dallas departures each day: daily_departures
daily_departures = dallas.resample('D').sum()

# Generate the summary statistics for daily Dallas departures: stats
stats = daily_departures.describe()

print(dallas.head(), daily_departures.head(), stats)

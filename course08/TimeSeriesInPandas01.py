import pandas as pd

file = "weather_data_austin_2010.csv"
df = pd.read_csv(
    file
)
print(df.head())

temperature_list = df['Temperature'].tolist()
date_list = df['Date'].tolist()

print(type(temperature_list), type(date_list))

print('-----------------EJERCICIO-----------------')


# Prepare a format string: time_format
time_format = '%Y-%m-%d %H:%M'

# Convert date_list into a datetime object: my_datetimes
my_datetimes = pd.to_datetime(date_list, format=time_format)

# Construct a pandas Series using temperature_list and my_datetimes: time_series
time_series = pd.Series(temperature_list, index=my_datetimes)

print(time_series.head())


print('---------------- example--------------------')

date_list = ['2015-01-01 091234', '2015-Jan-01 09:1:34']
date_list = pd.to_datetime(date_list, format='%Y-%m-%d %H%M%S')
print(date_list)

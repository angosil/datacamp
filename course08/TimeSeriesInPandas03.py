import pandas as pd

date_range_1 = pd.date_range('2016-07-01', periods=17, freq='D')
print(date_range_1)
ts1 = pd.Series([x for x in range(0, 17)], index=date_range_1)
print(ts1)

date_range_2 = pd.date_range('2016-07-01', periods=10, freq='B')
print(date_range_2)
ts2 = pd.Series([x for x in range(0, 10)], index=date_range_2)
print(ts2)

print('-----------------EJERCICIO-----------------')

# Reindex without fill method: ts3
ts3 = ts2.reindex(ts1.index)
print(ts3)
print('-----------------ts3-----------------------')
# Reindex with fill method, using forward fill: ts4
ts4 = ts2.reindex(ts1.index, method="ffill")
print(ts4)
print('-----------------ts4-----------------------')
# Combine ts1 + ts2: sum12
sum12 = ts1 + ts2
print(sum12)
print('-----------------sum12-----------------------')
# Combine ts1 + ts3: sum13
sum13 = ts1 + ts3
print(sum13)
print('-----------------sum13-----------------------')
# Combine ts1 + ts4: sum14
sum14 = ts1 + ts4
print(sum14)
print('-----------------sum14-----------------------')

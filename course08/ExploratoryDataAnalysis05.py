import pandas as pd
import matplotlib.pyplot as plt


file = "titanic.csv"
df = pd.read_csv(
    file,
    header=0
)
print(df.head())
print('-----------------EJERCICIO-----------------')

# Print summary statistics of the fare column with .describe()
print(df.fare.describe())

# Generate a box plot of the fare column
df.fare.plot(kind='box')

# Show the plot
plt.show()

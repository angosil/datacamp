import pandas as pd
# Import numpy
import numpy as np

df = pd.read_csv('world_population.csv')
df.info()

# Create array of DataFrame values: np_vals
print('---values :----')
print(df.values)
np_vals = df.values

# Create new array of base 10 logarithm values: np_vals_log10
print('----log10 :----')
print(np.log10(np_vals))
np_vals_log10 = np.log10(np_vals)

# Create array of new DataFrame by passing df to np.log10(): df_log10
print('---df log10 :---')
print(np.log10(df))
df_log10 = np.log10(df)

# Print original and new data containers
print(type(np_vals), type(np_vals_log10))
print(type(df), type(df_log10))

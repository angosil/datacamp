import pandas as pd
import numpy as np

file = "austin_airport_departure_data_2015_july.csv"
df = pd.read_csv(
    file,
    header=10
)
print(df.head())
# clear withe space in df titles row
df.columns = df.columns.str.strip()
# print(df.columns.tolist())

print('-----------------EJERCICIO-----------------')

# Buid a Boolean mask to filter out all the 'LAX' departure flights: mask
mask = df['Destination Airport'] == 'LAX'
# print(mask)

# Use the mask to subset the data: la
la = df[mask]
print('---la.head()---')
print(la.head())

# Combine two columns of data to create a datetime series: times_tz_none
times_tz_none = pd.to_datetime(la['Date (MM/DD/YYYY)'] + ' ' + la['Wheels-off Time'])
print('---times_tz_none.head()---')
print(times_tz_none.head())

# Localize the time to US/Central: times_tz_central
times_tz_central = times_tz_none.dt.tz_localize('US/Central')

# Convert the datetimes from US/Central to US/Pacific
times_tz_pacific = times_tz_central.dt.tz_convert('US/Pacific')

print(type(la))

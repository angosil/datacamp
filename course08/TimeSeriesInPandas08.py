import pandas as pd
import matplotlib.pyplot as plt


file = "weather_data_austin_2010.csv"
df_1 = pd.read_csv(
    file
)

df = df_1.loc[:, ('Temperature', 'Date')]

print(df.head())

print('-----------------EJERCICIO-01-----------------')

# Plot the raw data before setting the datetime index
df.plot()
plt.show()

# Convert the 'Date' column into a collection of datetime objects: df.Date
df.loc[:, 'Date'] = pd.to_datetime(df.loc[:, 'Date'])

# Set the index to be the converted 'Date' column
df.set_index('Date', inplace=True)

# Re-plot the DataFrame to see that the axis is now datetime aware!
df.plot()
plt.show()

print(df.head())

import pandas as pd

file = "weather_data_austin_2010.csv"
df = pd.read_csv(
    file,
    parse_dates=True,
    index_col="Date"
)
print(df.head())

ts0 = df['Temperature']

print(ts0.head())

print('-----------------EJERCICIO-----------------')

# Extract the hour from 9pm to 10pm on '2010-10-11': ts1
ts1 = ts0.loc['2010-10-11 21:00:00']

# Extract '2010-07-04' from ts0: ts2
ts2 = ts0.loc['2010-07-04']

# Extract data from '2010-12-15' to '2010-12-31': ts3
ts3 = ts0.loc['2010-12-15':'2010-12-31']

print(ts1, ts2, ts3)

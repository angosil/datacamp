import pandas as pd

df_1 = pd.read_csv('world_ind_pop_data.csv')
df_1.info()
print(df_1.values)

df_total_population = df_1[['Year', 'Total Population']]

df_total_population.loc[df_total_population['Year'] <= 2010]

df_total_population.info()
print(df_total_population.tail())

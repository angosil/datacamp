import pandas as pd

list_keys = ['Country', 'Total']
list_values = [['United States', 'Soviet Union', 'United Kingdom'], [1118, 473, 273]]

# Zip the 2 lists together into one list of (key,value) tuples: zipped
zipped = list(zip(list_keys, list_values))

# Inspect the list using print()
print(zipped)

# Build a dictionary with the zipped list: data
data = dict(zipped)

# Build and inspect a DataFrame from the dictionary: df
df = pd.DataFrame(data)
print(df)

list_keys_billboard = ['a', 'b', 'c', 'd']
list_billboard = [
    [1980,1981, 1982],
    ['Blondie', 'Chistorpher Cross', 'Joan Jett'],
    ['Call Me', 'Arthurs Theme', 'I Love Rock and Roll'],
    [6, 3, 7]
]
list_billboard_zipped = list(zip(list_keys_billboard, list_billboard))
dict_billboard = dict(list_billboard_zipped)


df_1 = pd.DataFrame(dict_billboard)
df_1.info()
print(df_1.head())

# Build a list of labels: list_labels
list_labels = ['year', 'artist', 'song', 'chart weeks']

# Assign the list of labels to the columns attribute: df.columns
df_1.columns = list_labels

print(df_1.head())

import pandas as pd
import matplotlib.pyplot as plt

file = "weather_data_austin_2010.csv"
new_labels = ['Temperature (deg F)', 'Dew Point (deg F)', 'Pressure (atm)', 'Date']
df1 = pd.read_csv(file, header=0, names=new_labels)
df = df1.iloc[:, [0, 1, 2]]
print(df.head())

# Plot all columns (default)
# plt.plot(df['Temperature'].values)
df.plot()
plt.show()

# Plot all columns as subplots
df.plot(subplots=True)
plt.show()

# Plot just the Dew Point data
column_list1 = ['Dew Point (deg F)']
df[column_list1].plot()
plt.show()

# Plot the Dew Point and Temperature data, but not the Pressure data
column_list2 = ['Temperature (deg F)', 'Dew Point (deg F)']
df[column_list2].plot()
plt.show()

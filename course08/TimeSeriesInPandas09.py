import pandas as pd
import matplotlib.pyplot as plt


file = "weather_data_austin_2010.csv"
df = pd.read_csv(
    file,
    parse_dates=True,
    index_col="Date",
)
print(df.head())

print('-----------------EJERCICIO-01-----------------')

# Plot the summer data
df.Temperature['2010-Jun':'2010-Aug'].plot()
plt.show()
plt.clf()

# Plot the one week data
df.Temperature['2010-06-10':'2010-06-17'].plot()
plt.show()
plt.clf()

import pandas as pd
import matplotlib.pyplot as plt


file_messy = "percent-bachelors-degrees-women-usa.csv"
df = pd.read_csv(
    file_messy,
    header=0,
    index_col='Year'
)
print(df.head())
print('----------------------------------')

# # Print the minimum value of the Engineering column
print(df.Engineering.min())
#
# # Print the maximum value of the Engineering column
print(df.Engineering.max())
#
# # Construct the mean percentage per year: mean
mean = df.mean(axis='columns')
#
# # Plot the average percentage per year
mean.plot()
#
# # Display the plot
plt.show()

int_list = [5, -2, -4, 6]
print(n for n in int_list if n < 0)

code_names = ['Spartan', 'Green Arrow']
characters_names = ['John Diggle', 'Oliver Queen']

team_arrow = zip(characters_names, code_names)
names, cnames = zip(*team_arrow)
print(cnames)

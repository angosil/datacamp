from setuptools import setup

setup(
    name="cuentas",
    version='0.1',
    py_modules=['cuentas'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        cuentas=cuentas:cli
    ''',
)

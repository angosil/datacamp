# Import click
import click
# Import pandas
import pandas as pd

@click.command()
@click.option('--name', default='', help='Who are you?')
@click.argument('file')
def cli(name, file):
    """Example script."""
    if not name:
        name = "Someone"
    click.echo("Hello {0}".format(name))

    # Assign spreadsheet filename: file
    # Load spreadsheet: xl
    xl = pd.ExcelFile(file)
    click.echo(xl.sheet_names)

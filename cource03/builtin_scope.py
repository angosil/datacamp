import builtins

builtins_list = dir(builtins)

builtins_to_find = ['sum', 'range', 'array', 'tuple']

for entry in builtins_to_find:
    if entry in builtins_list:
        print(entry + ' is in builtins_list')
    else:
        print(entry + ' is not in builtins_list')

print(builtins_list)
print(builtins_to_find)

# Define shout_echo
def shout_echo(word1, echo=1, intense=False):
    """Concatenate echo copies of word1 and three
    exclamation marks at the end of the string."""

    # Initialize empty strings: echo_word, shout_words
    echo_word = ""
    echo_word_new = ""

    try:
        # Raise an error with raise
        if echo < 0:
            raise ValueError

        # Concatenate echo copies of word1 using *: echo_word
        echo_word = word1 * echo

        # Capitalize echo_word if intense is True
        if intense is True:
            # Capitalize and concatenate '!!!': echo_word_new
            echo_word_new = echo_word.upper() + '!!!'
        else:
            # Concatenate '!!!' to echo_word: echo_word_new
            echo_word_new = echo_word + '!!!'
    except TypeError:
        # Print error message
        print("word1 must be a string and echo must be an integer.")
    except ValueError:
        # Print error message
        print("echo must be greater than 0")

    # Return echo_word
    return echo_word_new


# Call shout_echo() with "Hey", echo=5 and intense=True: with_big_echo
with_big_echo = shout_echo("Hey", 5)

# Call shout_echo() with "Hey" and intense=True: big_no_echo
big_no_echo = shout_echo("Hey", intense=True)

# Print values
print(with_big_echo)
print(big_no_echo)

# Call shout_echo
shout_echo("particle", -3)

# Call shout_echo
shout_echo("particle", echo="accelerator")

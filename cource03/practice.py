x = ['sat', 'sun', 'fri', 'sat', 'sun', 'fri', 'fri', 'sat']
counts = {}
for val in x:
    if val in counts.keys():
        counts[val] += 1
    else:
        counts[val] = 1
print(counts)


def nth_root(n):
    """Returns the actual_root function"""

    def actual_root(x):
        """Returns the nth root of x"""
        root = x ** (1 / n)
        return root

    return actual_root


square_root = nth_root(2)
cube_root = nth_root(3)
print(square_root(9), cube_root(27))

x = (2, 1, 1)
print(type(x))


def easy_print(**a):
    for p, q in a.items():
        print('The value of ' + str(p) + " is " + str(q))


easy_print(x=15, y=30)


def square(s):
    """Returns the area and perimeter of a square"""
    try:
        a = s * s
        p = 4 * s
        return a, p
    except TypeError:
        print("s can not be a string")

square('5')
